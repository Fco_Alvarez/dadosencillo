// Carlos Francisco Alvarez Salgado
// Tijuana B.C. Enero 2021
// Generador de Numeros pseudo-aleatorios
// El Plan:
// Vamos a generar múltiples repeticiones con la secuencia de números pseudo-aleatorios
// usando una semilla fija, registrándolos para compararlos con repeticiones con 
// una semilla obtenida aleatoriamente por la entrada análoga flotante o no conectada.

// Procedimiento propuesto:
// Registrar 5 corridas por semilla inicial y los resultados almacenarlos en un 
// archivo separado por comas para poder analizarse posteriormente en alguna 
// aplicación de hoja de cálculo.



#include <Adafruit_NeoPixel.h>

#define NUM_LEDS 7 // 7 Pixeles por dado
#define PIN_Dados 3 // Pin conectado a la tira de Neopixeles
#define pinBoton 2 // PushBotton
#define btnModo 8 // Selecciona modo de Semilla
#define BRIGHTNESS 255
#define TIROS 10 // Cantidad de tiros por presion de boton

Adafruit_NeoPixel luces = Adafruit_NeoPixel(NUM_LEDS, PIN_Dados, NEO_GRB + NEO_KHZ800);

// Configuracion de Neopixeles
int8_t Intencidad=255;
// Color neopixel
int8_t cR=250;
int8_t cG=50;
int8_t cB=10;
int8_t dado1=0;

// Semilla inicial para Random
int8_t semilla=4;

int8_t dibujo[7][7]{
  {0,0,0,0,0,0,0}, //Apagado
  {0,0,0,1,0,0,0}, // 1
  {1,0,0,0,0,0,1}, // 2
  {1,0,0,1,0,0,1}, // 3
  {1,0,1,0,1,0,1}, // 4
  {1,0,1,1,1,0,1}, // 5
  {1,1,1,0,1,1,1}  // 6
};

void setup() {  
  luces.setBrightness(BRIGHTNESS);
  luces.begin();
  luces.show(); // inicia luces apagadas
  //Activar interrupcion en boton
  pinMode(btnModo, INPUT_PULLUP);
  pinMode(pinBoton, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinBoton), isrBoton, FALLING );
  // Activar monitor serial
  Serial.begin(9600);
}

// Vector de interrupciones para boton
void isrBoton(){
  while(!digitalRead(pinBoton));
  // Se activo dado
  // Usando entrada analoga sin conectar como semilla
  if (!digitalRead(btnModo)) semilla=analogRead(0); 
    
  randomSeed(semilla); // Usando semilla
  
  Serial.print("Boton Presionado... tirando ");
  Serial.print(TIROS);
  Serial.print(" veces con semilla ");
  Serial.println(semilla);
  for(int8_t i=1;i<=TIROS;i++){
    dado1=random(1,7);
    iluminaLuces();
    Serial.print(dado1);
    Serial.print((i<TIROS) ? "," : "\n"); // Poner comas a valores intermedios
    delay(100); // Para ver el resultado por un corto tiempo
  }
}

// Dibuja el o los dados sobre los NeoPixeles
void iluminaLuces(){
  // Dibujar dado 1
  // recorre puntos del dado
  for (int8_t i=0; i<=7;i++){
    int x=dibujo[dado1][i];
  	luces.setPixelColor(i,luces.Color(x*cR,x*cG,x*cB));
  }
  // Dibujar dado 2 si deseas extender a mas dados
  //for (int8_t i=0; i<=7;i++){
    //int x=dibujo[dado2][i];
  	//luces.setPixelColor(i+7,luces.Color(x*cR,x*cG,x*cB));
  //}
  
  luces.show();
}


void loop() {
    
}